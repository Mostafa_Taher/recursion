First program contains multiplication function with recursion of log n steps.
Second program contains Currency function that convert a given amount from one currency to another among a list of possible currencies.
Recursion is an elegant technique if used correctly. If used wrongly it can give very poor performance.
It makes many codes easier.

 *************************************
 
||program submitted by : Mostafa Taher||
||University : Cairo University _ Egypt||
||Faculty of computer  & information||

 **************************************